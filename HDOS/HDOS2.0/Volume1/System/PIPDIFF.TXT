07/13/19  09:42:10
Comparing PIP.ABS and MYPIP.ABS
File 1 is: an ABS file
	Load Address: 042.200
	Length:       021.342  (4578)
	Entry Point:  064.034
File 2 is: an ABS file
	Load Address: 042.200
	Length:       025.056  (5422)
	Entry Point:  064.034

From    To  Count    ORG     ***     File 1      ***   ***     File 2      ***
====   ==== =====  =======   === === === === === ===   === === === === === ===
*
*	Our version is 844 bytes larger than the distribution copy. This is due
*	to buffer space that can be stripped from the end.  Also the distribution
*	copy has a Patch History Table, which our version doesn't have.
*
0004 - 0005     2  042.174   342 021 000 000 000 000   056 025 000 000 000 000 
*
*   044.374              01395   COPYD	DS	FB.NAML		HOLD AREA FOR WILDCARD DESTINATION
*
0284 - 0294    17  044.374   151 156 040 162 145 155   060 021 000 007 064 374 
*
*   045.327              02347   RENA	DS	FB.NAML		FILE NAME WORK AREA
*
035F - 036F    17  045.327   112 107 114 051 051 012   072 350 063 247 300 076 
*
*   047.120              02618   LSTD	DS	24		FILE NAME DECODE AREA
*
04D8 - 04EF    24  047.120   142 171 040 107 101 103   152 047 315 234 030 002 
*
*   047.211              02622   LSTG1	DS	9		DATE
*
0511 - 0519     9  047.211   056 054 040 061 071 070   377 004 321 332 366 046 
*
*   053.021              03059   AENA	DS	FB.NAML
*
0899 - 08A9    17  053.021   146 040 112 107 114 051   101 154 154 040 106 151 
*
*   055.015              03525   DNTA	DS	9		WORK AREA
*
0A95 - 0A9D     9  055.015   070 060 012 040 050 142   000 257 303 024 054 076 
*
*   056.064              03759   EWSB	DS	30
*   056.122              03761   EWSC	DS	8+3		WILDCARD PATTERN FOR DIRECTORY SEARCH
*
0BBC - 0BE4    41  056.064   157 146 040 112 107 114   031 345 341 021 372 377 
*
*   063.162              05662   PATCH	DS	64		PATCH AREA
*
10FA - 1139    64  063.162   040 112 107 114 051 051   063 301 303 025 062 321 
*
*   064.007              05855   	DS	FB.NAML		NAME AREA
*
118F - 119F    17  064.007   162 151 147 150 164 040   021 147 063 006 005 176 

File 2 is longer

