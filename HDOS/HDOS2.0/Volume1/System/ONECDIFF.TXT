07/14/19  10:58:23
Comparing ONECOPY.ABS and MYONECPY.ABS
File 1 is: an ABS file
	Load Address: 042.200
	Length:       022.346  (4838)
	Entry Point:  063.272
File 2 is: an ABS file
	Load Address: 042.200
	Length:       023.314  (5068)
	Entry Point:  063.272

From    To  Count    ORG     ***     File 1      ***   ***     File 2      ***
====   ==== =====  =======   === === === === === ===   === === === === === ===
*
* ONECOPY.ABS is shorter because it has buffer space trimmed from the end.  We
* could trim the last 230 bytes to be consistent...
*
0004 - 0005     2  042.174   346 022 000 000 000 000   314 023 000 000 000 000 
*
*   044.205              01624   OCOPYD	DS	FB.NAML		HOLD AREA FOR WILDCARD DESTINATION
*
020D - 021D    17  044.205   160 145 143 151 146 171   215 043 075 076 204 302 
*
*   050.230              02618   LSTD	DS	24		FILE NAME DECODE AREA
*
0620 - 0637    24  050.230   167 040 120 151 164 143   050 315 027 041 315 052 
*
*   050.321              02622   LSTG1	DS	9		DATE
*
0659 - 0661     9  050.321   141 040 120 151 164 056   315 234 056 076 002 325 
*
*   053.232              03059   AENA	DS	FB.NAML
*
0922 - 0932    17  053.232   021 136 055 041 011 000   234 052 365 315 136 031 
*
*   055.135              03525   DNTA	DS	9		WORK AREA
*
0AE5 - 0AED     9  055.135   156 040 162 145 155 145   060 257 303 144 054 076 
*
*   056.163              03759   EWSB	DS	30
*   056.221              03761   EWSC	DS	8+3		WILDCARD PATTERN FOR DIRECTORY SEARCH
*
0BFB - 0C23    41  056.163   156 143 145 040 157 146   246 021 010 000 031 246 
*
*   061.310              05137   PATCH	DS	64		PATCH AREA
*   000.000              05138   	IF	ONECOPY							/2.0a/
*   062.010              05139   	DS	*+255/256*256-* Auxiliary Patch Area (Round up 1 page)	/2.0a/
*                        05140   	ENDIF								/2.0a/
0F50 - 0F60    17  061.310   142 171 040 107 101 103   060 303 252 030 112 141 
0F62 - 0F7F    30  061.332   156 143 145 040 157 146   171 112 165 156 112 165 
0F81 - 1011   145  061.371   110 105 101 124 110 040   315 072 030 345 365 345 
1013 - 1087   117  062.213   071 070 060 012 040 050   302 174 061 315 270 061 
*
*   063.243              05339   	DS	FB.NAML		NAME AREA
*
112B - 113B    17  063.243   050 151 156 040 162 145   315 216 030 302 240 061 
*
* MEML=65.146 - all buffer space after this point (can be stripped from .ABS file )
*
12EE - 131E    49  065.146   103 117 056 054 040 061   144 040 164 150 145 040 
1320 - 1345    38  065.230   110 105 101 124 110 040   164 150 145 040 151 156 
1347 - 139C    86  065.277   040 157 146 040 112 107   156 040 162 145 141 144 
139E - 13B5    24  066.026   147 150 164 040 050 103   163 153 163 072 012 212 
13B7 - 13F2    60  066.057   050 142 171 040 107 101   104 151 163 153 050 163 

File 1 is longer (due to buffer space)

